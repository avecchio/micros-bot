/**** 
 * PIN DEFINITIONS 
****/
const int FRONT_SONAR_ECHO = 6;
const int FRONT_SONAR_TRIG = 7;

const int RIGHT_SONAR_ECHO = 8;
const int RIGHT_SONAR_TRIG = 9;

const int BACK_SONAR_ECHO = 10;
const int BACK_SONAR_TRIG = 11;

const int LEFT_SONAR_ECHO = 12;
const int LEFT_SONAR_TRIG = 13;

// Bluetooth (not implemented)
const int BT_TXD = 4;
const int BT_RXD = 5;

// motor one
const int enA = A4;
const int in1 = A0;
const int in2 = A1;

// motor two
const int enB = A5;
const int in3 = A2;
const int in4 = A3;

/**** 
 * Motor directions (forward, reverse, stop)
****/
const int MOTOR_FORWARD = 1;
const int MOTOR_REVERSE = -1;
const int MOTOR_STOP = 0;

/**** 
 * Robot directions (forward, reverse, clockwise, counterclockwise, stop)
****/
const String COMMAND_FORWARD = "FW";
const String COMMAND_REVERSE = "RV";
const String COMMAND_CW = "CW";
const String COMMAND_CCW = "CCW";
const String COMMAND_STOP = "ST";

/**** 
 * Job Queue Definition
****/
#define QUEUE_SIZE 128

/*Declaration of circular queue.*/
typedef struct {
    int front   ;
    int rear    ;
    int count   ;
    String elem[QUEUE_SIZE]    ;
} CirQueue;

/**** 
 * PIN DEFINITIONS 
****/
String mode = "sleeping";
String command = "";
int power = 0;
int move_counter = 0;

String instruction;
CirQueue q;

int maximumRange = 200; // Maximum range in cm
int minimumRange = 0; // Minimum range in cm

/**** 
 * PIN DEFINITIONS 
****/
/*Initailization of circular queue.*/
void initCirQueue(CirQueue * q) {
    q->front =  0;
    q->rear  = -1;
    q->count =  0;
}

/*Check Queue is full or not*/
int isFull(CirQueue * q) {
    int full=0;
    if(q->count == QUEUE_SIZE)
        full = 1;

    return full;
}

/*Check Queue is empty or not*/
int isEmpty(CirQueue * q) {
    int empty=0;
    if(q->count == 0)
        empty = 1;

    return empty;
}

/*To insert item into circular queue.*/
void insertCirQueue(CirQueue * q, String item) {
    if( isFull(q) ) {
        return;
    }

    q->rear = (q->rear+1)%QUEUE_SIZE;
    q->elem[q->rear] = item;
    q->count++;
}

/*To delete item from queue.*/
int deleteCirQueue(CirQueue * q, String *item) {
    if( isEmpty(q) ) {
        return -1;
    }

    *item    = q->elem[q->front];
    q->front = (q->front+1)%QUEUE_SIZE;
    q->count--;

    return 0;
}

/**** 
 * PIN DEFINITIONS 
****/
unsigned long ping(unsigned int echo_pin, unsigned int trigger_pin){
  long duration, distance;

  digitalWrite(trigger_pin, LOW);
  delayMicroseconds(2);

  digitalWrite(trigger_pin, HIGH);
  delayMicroseconds(10);

  digitalWrite(trigger_pin, LOW);
  duration = pulseIn(echo_pin, HIGH);

  //Calculate the distance (in cm) based on the speed of sound.
  distance = duration/58.2;
  if (distance >= maximumRange || distance <= minimumRange){
    return -1;
  }
  else {
    return distance;
  }

}

/**** 
 * PIN DEFINITIONS 
****/
void drive(int right, int left, int speed, int timeout){
  String h = String(right) + "," + String(left) + "," + String(speed) + "," + String(timeout);
  Serial.println("MV," + h);

  if(right == MOTOR_FORWARD){
    digitalWrite(in1, HIGH);
    digitalWrite(in2, LOW);
  }
  else if(right == MOTOR_REVERSE){
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);
  }
  else if(right == MOTOR_STOP){
    digitalWrite(in1, LOW);
    digitalWrite(in2, LOW);
  }
  analogWrite(enA, speed);

  if(left == MOTOR_FORWARD){
    digitalWrite(in3, HIGH);
    digitalWrite(in4, LOW);
  }
  else if(left == MOTOR_REVERSE){
    digitalWrite(in3, LOW);
    digitalWrite(in4, HIGH);
  }
  else if(left == MOTOR_STOP){
    digitalWrite(in3, LOW);
    digitalWrite(in4, LOW);
  }
  analogWrite(enB, speed);

  delay(timeout);
}

void move(String instruction, int speed, int delay){
  if(instruction ==  COMMAND_CW){
    drive(MOTOR_REVERSE, MOTOR_FORWARD, speed, delay);
  }
  else if(instruction == COMMAND_CCW){
    drive(MOTOR_FORWARD, MOTOR_REVERSE, speed, delay);
  }
  else if(instruction == COMMAND_FORWARD){
    drive(MOTOR_FORWARD, MOTOR_FORWARD, speed, delay);
  }
  else if(instruction == COMMAND_REVERSE){
    drive(MOTOR_REVERSE, MOTOR_REVERSE, speed, delay);
  }
  else if(instruction == COMMAND_STOP){
    drive(MOTOR_STOP, MOTOR_STOP, speed, delay);
  }
}

void setup() {
  Serial.begin(9600);
  delay(100);

  initCirQueue(&q);

  pinMode(FRONT_SONAR_ECHO, INPUT);
  pinMode(FRONT_SONAR_TRIG, OUTPUT);

  pinMode(RIGHT_SONAR_ECHO, INPUT);
  pinMode(RIGHT_SONAR_TRIG, OUTPUT);

  pinMode(BACK_SONAR_ECHO, INPUT);
  pinMode(BACK_SONAR_TRIG, OUTPUT);

  pinMode(LEFT_SONAR_ECHO, INPUT);
  pinMode(LEFT_SONAR_TRIG, OUTPUT);

  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

}

unsigned long sonar_front, sonar_back, sonar_left, sonar_right;
String last_uid;
void loop() {

  Serial.println("Echo");

  sonar_front = ping(FRONT_SONAR_ECHO, FRONT_SONAR_TRIG);
  sonar_back = ping(RIGHT_SONAR_ECHO, RIGHT_SONAR_TRIG);
  sonar_left = ping(BACK_SONAR_ECHO, BACK_SONAR_TRIG);
  sonar_right = ping(LEFT_SONAR_ECHO, LEFT_SONAR_TRIG);
  const int proxy = 3;
  boolean front_proximity = (sonar_front <= proxy);
  boolean back_proximity = (sonar_back <= proxy);
  boolean left_proximity = (sonar_left <= proxy);
  boolean right_proximity = (sonar_right <= proxy);
  boolean proximity = (front_proximity || back_proximity || left_proximity || right_proximity);

  if(proximity){
    move(COMMAND_STOP, 0, 500);
    mode = "sleeping";
    Serial.println("EM,estopped");
    for(int i=0; i<QUEUE_SIZE; i++){
      deleteCirQueue(&q, &instruction);
    }
    if(front_proximity){
      mode = "moving";
      move(COMMAND_FORWARD, 100, 400);      
    }
    else if(back_proximity){
      mode = "moving";
      move(COMMAND_REVERSE, 100, 400);          
    }
  }

  String front_bytes = String(sonar_front);
  String back_bytes = String(sonar_back);
  String left_bytes = String(sonar_left);
  String right_bytes = String(sonar_right);

  String sensor_data = "TX,";
  sensor_data = sensor_data + front_bytes + "," + right_bytes + ",";
  sensor_data = sensor_data + left_bytes + "," + back_bytes;
  Serial.println(sensor_data);

  // send data only when you receive data:
  if(Serial.available()){
    String start = Serial.readStringUntil(',');
    String uid = Serial.readStringUntil(',');
    String cmd = Serial.readStringUntil(',');
    String sp = Serial.readStringUntil(',');
    String tmout = Serial.readStringUntil(';');
      if(start == "RM"){
        String stored_command = cmd + "," + sp + "," + tmout;
        Serial.println(stored_command);
        insertCirQueue(&q, stored_command);
        Serial.println("CX");

        last_uid = uid;
      }
  }

  boolean runmode = false;
  if(mode == "sleeping"){
    if ( deleteCirQueue( &q, &instruction ) == 0 ) {
      int first_separator = instruction.indexOf(',');
      int second_separator = instruction.indexOf(',', first_separator+1);
      
      command = instruction.substring(0,first_separator);
      power = instruction.substring(first_separator+1, second_separator).toInt();
      move_counter = instruction.substring(second_separator + 1).toInt();
      mode = "moving";
    }
  }
  else{
      if(move_counter == 0){
        move("ST",0,10);
        Serial.println("EM,"+command);
        mode = "sleeping";
      }
      else{
        Serial.println("moved");
        move(command, power, 10);
        move_counter = move_counter - 10;
        mode = "moving";
      }
  }

}
