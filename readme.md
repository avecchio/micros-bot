# Micros Bot

This is the term project for CE-420. This is a simple robot used to be a distraction for pets.
The robot will respond to close objects and move in the opposite direction.

This project utilizes arduino and python. Arduino is the microcontroller
and python is the decision engine. The python code is executed on a raspberry pi.
