# import the necessary packages
from dropbox.client import DropboxOAuth2FlowNoRedirect
from dropbox.client import DropboxClient
from picamera.array import PiRGBArray
from picamera import PiCamera
import warnings
import json

import datetime
import imutils
import thread
import time
import cv2
import sys

import uuid
import os

class TempImage:
	def __init__(self, basePath="./", ext=".jpg"):
		# construct the file path
		self.path = "{base_path}/{rand}{ext}".format(base_path=basePath,
			rand=str(uuid.uuid4()), ext=ext)

	def cleanup(self):
		# remove the file
		os.remove(self.path)

class Camera:
    def __init__(self):
        self.camera = cv2.VideoCapture(0)
        self.face_detected = False
        self.motion_detected = False
        self.face_video_capture = None

    def is_motion_detected(self):
        return self.motion_detected

    def is_face_detected(self):
        return self.face_detected

    def noface(self):
        self.face_video_capture.release()

    def nocamera(self):
        cv2.destroyAllWindows()

    def face_scanner(self,cascPath, faceCascade):
        while True:
            print("hi")
            # Capture frame-by-frame
            ret, frame = self.face_video_capture.read()

            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            faces = faceCascade.detectMultiScale(
                gray,
                scaleFactor=1.1,
                minNeighbors=5,
                minSize=(30, 30),
                flags=cv2.cv.CV_HAAR_SCALE_IMAGE
            )

            # Draw a rectangle around the faces
            for (x, y, w, h) in faces:
                cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

            # Display the resulting frame
            cv2.imshow('Video', frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break


    def face_detect(self):

        cascPath = sys.argv[1]
        faceCascade = cv2.CascadeClassifier(cascPath)

        self.face_video_capture = cv2.VideoCapture(0)
        try:
            thread.start_new_thread(self.face_scanner, (cascPath, faceCascade, ))
        except:
            pass
        # When everything is done, release the capture

    def motion_scanner(self, conf, camera, rawCapture, client, avg, lastUploaded, motionCounter):
        # capture frames from the camera
        for f in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        	# grab the raw NumPy array representing the image and initialize
        	# the timestamp and occupied/unoccupied text
        	frame = f.array
        	timestamp = datetime.datetime.now()
        	text = "Unoccupied"
                self.motion_detected = False

        	# resize the frame, convert it to grayscale, and blur it
        	frame = imutils.resize(frame, width=500)
        	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        	gray = cv2.GaussianBlur(gray, (21, 21), 0)

        	# if the average frame is None, initialize it
        	if avg is None:
        		print "[INFO] starting background model..."
        		avg = gray.copy().astype("float")
        		rawCapture.truncate(0)
        		continue

        	# accumulate the weighted average between the current frame and
        	# previous frames, then compute the difference between the current
        	# frame and running average
        	cv2.accumulateWeighted(gray, avg, 0.5)
        	frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(avg))

        	# threshold the delta image, dilate the thresholded image to fill
        	# in holes, then find contours on thresholded image
        	thresh = cv2.threshold(frameDelta, conf["delta_thresh"], 255,
        		cv2.THRESH_BINARY)[1]
        	thresh = cv2.dilate(thresh, None, iterations=2)
        	(cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
        		cv2.CHAIN_APPROX_SIMPLE)

        	# loop over the contours
        	for c in cnts:
        		# if the contour is too small, ignore it
        		if cv2.contourArea(c) < conf["min_area"]:
        			continue

        		# compute the bounding box for the contour, draw it on the frame,
        		# and update the text
        		(x, y, w, h) = cv2.boundingRect(c)
        		cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        		text = "Occupied"
                        self.motion_detected = True

        	# draw the text and timestamp on the frame
        	ts = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
        	cv2.putText(frame, "Room Status: {}".format(text), (10, 20),
        		cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
        	cv2.putText(frame, ts, (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX,
        		0.35, (0, 0, 255), 1)
        	# check to see if the room is occupied
        	if text == "Occupied":
        		# check to see if enough time has passed between uploads
        		if (timestamp - lastUploaded).seconds >= conf["min_upload_seconds"]:
        			# increment the motion counter
        			motionCounter += 1

        			# check to see if the number of frames with consistent motion is
        			# high enough
        			if motionCounter >= conf["min_motion_frames"]:
        				# check to see if dropbox sohuld be used
        				if conf["use_dropbox"]:
        					# write the image to temporary file
        					t = TempImage()
        					cv2.imwrite(t.path, frame)

        					# upload the image to Dropbox and cleanup the tempory image
        					print "[UPLOAD] {}".format(ts)
        					path = "{base_path}/{timestamp}.jpg".format(
        						base_path=conf["dropbox_base_path"], timestamp=ts)
        					client.put_file(path, open(t.path, "rb"))
        					t.cleanup()

        				# update the last uploaded timestamp and reset the motion
        				# counter
        				lastUploaded = timestamp
        				motionCounter = 0

        	# otherwise, the room is not occupied
        	else:
        		motionCounter = 0
        	# check to see if the frames should be displayed to screen
        	if conf["show_video"]:
        		# display the security feed
        		cv2.imshow("Security Feed", frame)
        		key = cv2.waitKey(1) & 0xFF

        	# if the `q` key is pressed, break from the lop
        	if key == ord("q"):
        		break

        	# clear the stream in preparation for the next frame
        	rawCapture.truncate(0)

        # import the necessary packages


    def motion_detect(self):
        # import the necessary packages
        #from pyimagesearch.tempimage import TempImage

        conf = json.load(open("conf.json"))
        # initialize the camera and grab a reference to the raw camera capture
        camera = PiCamera()
        camera.resolution = tuple(conf["resolution"])
        camera.framerate = conf["fps"]
        rawCapture = PiRGBArray(camera, size=tuple(conf["resolution"]))

        client = None

        # allow the camera to warmup, then initialize the average frame, last
        # uploaded timestamp, and frame motion counter
        print "[INFO] warming up..."
        time.sleep(conf["camera_warmup_time"])
        avg = None
        lastUploaded = datetime.datetime.now()
        motionCounter = 0

        try:
            thread.start_new_thread(self.motion_scanner, (conf, camera, rawCapture, client, avg, lastUploaded, motionCounter, ))
        except Exception as err:
            print("Could not start camera")
            print(err)
