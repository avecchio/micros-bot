import random

class Movement(object):
    def __init__(self):
        self.sonars = {"front" : 0,"left" : 0,"right" : 0,"back" : 0}
        self.ticks = {"front" : 0,"left" : 0,"right" : 0,"back" : 0}
        self.state = "sleeping"
        self.moves = ["FW", "RV", "CW", "CCW", "ST"]
        self.communication = "None"

    def response(self, sensors, motion_detected, face_detected):
        front = self.detect("front", int(sensors[0]))
        left = self.detect("left", int(sensors[1]))
        right = self.detect("right", int(sensors[2]))
        back = self.detect("back", int(sensors[3]))

        move_commands = []
        # If anything has been detected, monitor the area
        if((self.state == "sleeping") and (front or left or right or back)):
            move_commands.append(self.move(self.moves[2],1,80))
            self.state = "monitoring"

        elif(self.state is "sleeping"):
            # Randomly move based on statistical chance
            chance = random.randrange(0,100000000000)
            if(chance > 99999999980):
                self.state = "relocating"

        elif(self.state is "monitoring"):
            # If motion was detected
            if motion_detected:
                self.state = "avoiding"
            # Or if anything detected in front of the sensors
            elif (int(sensors[0]) < 12 or int(sensors[1]) < 12 or int(sensors[2]) < 12 or int(sensors[3]) < 12):
                self.state = "avoiding"

        # Avoid the approaching body
        elif(self.state is "avoiding"):
            # Generate several move commands (random amount)
            move_counter = random.randrange(14,18)
            while(move_counter > 0):
                # Get a move
                random_move = self.moves[random.randrange(0,3)]
                # Generate the power and timeout
                power = random.randrange(0,200)
                timeout = random.randrange(100,1000)
                # Generate the command and add it to an array
                move_commands.append(self.move(random_move, 50, 300))
                move_counter = move_counter - 1;
            self.state = "sleeping"

        elif(self.state is "relocating"):
            # Generate a few commands (random amount)
            move_counter = random.randrange(2,4)
            while(move_counter > 0):
                # Get a move
                random_move = self.moves[random.randrange(0,3)]
                # Generate the power and timeout
                power = random.randrange(0,200)
                timeout = random.randrange(100,1000)
                # Generate the command and add it to an array
                move_commands.append(self.move(random_move, 50, 300))
                move_counter = move_counter - 1;
            self.state = "sleeping"
        else:
            pass

        return move_commands

    def detect(self, name, sonar_val):
        # If the ultrasonic value is correct
        if(sonar_val < 4294967295):
            if(self.sonars[name] == 0):
                self.sonars[name] = sonar_val
            # If the ultrasonic value is less than the previous value
            # add a point
            elif sonar_val < self.sonars[name]:
                self.ticks[name] = self.ticks[name] + 1
            # If the ultrasonic value is greater than the previous value
            # subtract a point
            elif self.ticks[name] > 0:
                self.ticks[name] = self.ticks[name] - 1;
            else:
                pass
            self.sonars[name] = sonar_val
        print(self.ticks[name])

        # If an object is approaching, return true
        if self.ticks[name] > 3:
            self.ticks[name] = 0
            return True
        else:
            return False

    # Create string for move command
    def move(self, command, speed, timeout):
        uid = random.randrange(10000)
        command = "RM," + str(uid) + "," + command + "," + str(speed) + "," + str(timeout)
        return command
