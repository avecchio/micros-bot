import serial
from time import sleep

class Piserial(object):
    def __init__(self):
        self.ser = None
        # Try first connection on Raspberry Pi
        try:
            print("Setting connection")
            self.ser = serial.Serial(port='/dev/ttyAMA0',baudrate=9600, timeout=100)
        except:
            print("Error with serial connection")

        # Try second usb on Raspberry Pi
        if self.ser is None :
            print("Trying a second time")
            try:
                self.ser = serial.Serial(port='/dev/ttyACM0', baudrate=9600, timeout=100)
            except:
                print("Still an issue")

        # Try connection on a mac
        if self.ser is None:
            print("Trying on a MAC")
            try:
                self.ser = serial.Serial(port='/dev/cu.usbmodem1411', baudrate=9600, timeout=100)
            except:
                print("Nothing is working")

        # Reset the connection due to bug
        if self.ser.isOpen:
          self.ser.close()
        self.ser.open()

    # Send data to arduino
    def transmit(self, message):
        self.ser.write(message)

    # Get data from arduino
    def receive(self):
        serial_data = self.ser.readline()
        return serial_data

    # Close the serial connection
    def close(self):
        if self.ser is not None:
            self.ser.close();
