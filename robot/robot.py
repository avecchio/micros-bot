from piserial import Piserial as piserial
#from camera import Camera as camera
from movement import Movement as movement
import time
import random

serial_port = piserial()
decision = movement()
cam = camera();
#cam.face_scanner()

transmitted_commands = True

commands = []

while(1):
    # Transmit the latest command
    if(transmitted_commands):
        if(len(commands) > 0):
            serial_port.transmit(commands[0])
    # Get a response from the arduino
    response = serial_port.receive().rstrip("\r\n")
    if(response == "CX"):
        transmitted_commands = False
        if(len(commands) > 0):
            commands.pop(0)
    if(response == "EM,"):
        transmitted_commands = True
    print("Printing response")
    print(response)
    sensors = response.split(",")
    if((len(sensors) == 5) and (sensors[0] == "TX")):
        #motion = cam.is_motion_detected()
        motion = cam.motion_detect()
        #face = cam.is_face_detected()
        face = False
        sensors.pop(0)
        command = decision.response(sensors, motion, face)
        if(len(command) > 0):
            commands += command

serial_port.close();
